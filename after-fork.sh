#!/bin/bash

function generate_php_salt {
    mv wp-config.template.php backend/wp-config.php
    SALT=$(curl -L https://api.wordpress.org/secret-key/1.1/salt/)
    STRING='SALT'
    printf '%s\n' "g/$STRING/d" a "$SALT" . w | ed -s backend/wp-config.php
}

PHP_VERSION="$1"
echo PHP_VERSION="${PHP_VERSION}" > .env
rm -rf prebuild
rm after-fork.sh
mv .gitlab-ci.after-fork.yml .gitlab-ci.yml
generate_php_salt
