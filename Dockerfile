ARG PHP_VERSION
FROM registry.gitlab.com/mkjmdski/wordpress/${PHP_VERSION}/prebuild:latest
COPY --chown=www-data:www-data backend /app/backend