# Wordpress on Docker

Inspired by [official](https://hub.docker.com/_/wordpress/) wordpress docker image, but targets specifically against constant developer code base (meaning docker build process isn't generic for all php versions)

## 1. How to start project

1. Fork this repository
2. Change 'wordpress' in local.env and docker-compose.yaml for the name of your project

Now you have two options

### 1.1 Start new shop

1. Add fresh wordpress files to the `backend` directory
2. Set `local.env` variables
3. Start backend by running `docker-compose up --force-recreate --build -d` and afterwards install wordpress by running `docker-compose exec app sh /start.sh INSTALL`

### 1.2 Continue with existing WP

1. Add your existing presta project to the backend directory and continue development, using `wp-config.php` from the template

### 1.3 Set security variables

Use this magic command to generate new wordpress security variables

```sh
curl https://api.wordpress.org/secret-key/1.1/salt/ | sed 's/define//g' | sed "s/('//g" | sed "s/',/=/g" | sed 's/);//g' | sed 's/ //g'
```

Add them to your `wp-confg.php`

## 3. Runing container

Use env variables to control the application. If you see blank page check if UID of www-data inside container matchers UID of the user which runs as owner of backend directory volume on host machine

### 3.1 Env variables

#### 3.1.1 Local envs

All env variables are explained inside `local.env` file, use them to configure your containers.

#### 3.1.2 Stage envs

All env variables used to control container lifecycle on the stage environment are in `deployment/stage.yaml`. There are 3 additional comparing to the local.

`DUMP_FILE` - dump to be load after container comes into stage environment (by default `init.sql`)

`DUMP_HOST` - host hardcoded in dump file

`TARGET_HOST` - host to be used on remote

`LOAD_DUMP` - variable which defines if we should load the dump in the current deployment (by default true)

### 3.2 Container commands

#### 3.2.1 Local development

`GET_DUMP` - creates dump of the given name or with name of current timestamp

`LOAD_DUMP` - loads dump of the given name or `init.sql`

`INSTALL` - installs wordpress from wp-cli using env variables defined in `local.env` (beta)

#### 3.2.2 Universal commands

`RESET_DB` - Drops db and then creates new (or just creates if db didn't exist)

`CHECK_DB_EXIST` - checks if db defined in the env exists

#### 3.2.3 System commands

`RUN` - starts php-fpm with nginx from supervisord

`PREPARE_STAGE` - preprares envrionment for the stage deploy using previous commands. Algorithm:

```python
if LOAD_DUMP:
    if db_exist():
        drop_db()
    if not db_exist():
        create_db()
    dump_file.replace(DUMP_HOST, TARGET_HOST)
    load_dump(dump_file)
```